class Biblio
    attr_accessor :autores, :titulo, :serie, :editorial, :edicion, :fecha, :isbn
    
    def initialize(autores, titulo, serie=nil, editorial, edicion, fecha, isbn)
        @autores = autores
        @titulo = titulo
        @serie = serie;
        @editorial = editorial
        @edicion = edicion
        @fecha = fecha
        @isbn = isbn
    end

    def to_s
        "#{@autores}\n#{@titulo}\n#{@serie}\n#{@editorial}\n#{@edicion}\n#{@fecha}\nISBN: #{@isbn}"
    end
end

=begin
A = Biblio.new("juan, horse luis", "flores", "serie1", "santillana", "100", "21/6/2015", "0002B")
puts A.to_s
=end