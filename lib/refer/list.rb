require './lib/refer/node.rb'

class List
    include Enumerable
    attr_accessor :ini
    
    
    def initialize()
        @ini = nil
    end
    
    def push_front(value)
        aux = Node.new(value, @ini)
        @ini = aux
    end
=begin 
    def push_front(collect)
        collect.each do |i|
            aux = Node.new(i, nil)
            aux.next = @ini
            @ini = aux
        end
    end
=end
    
    def push_back(value)
        aux = Node.new(value, nil)
        p = @ini
        while (p.next != nil)
            p = p.next
        end
        p.next = aux
    end
    
    def pop_front()
       aux = @ini.value
        if(@ini.next == nil)
            @ini = nil
        else
            @ini = @ini.next
        end
       return aux
    end
    
    def pop_back()
        p = @ini
        aux = p
        while (p.next != nil)
            aux = p
            p = p.next
        end
        aux.next = nil
        return p.value
    end
    
    def empty()
        return @ini == nil
    end

    def each
        aux = @ini
        while(aux != nil)
            yield aux.value
            aux = aux.next
        end
    end

end