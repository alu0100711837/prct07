Node = Struct.new(:value, :next)

class Node
    include Comparable
    def <=> other
        value <=> other.value
    end
end