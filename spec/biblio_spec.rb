require './lib/refer/biblio.rb'
require 'refer'

describe Refer do 
	before :each do
		@r=Biblio.new ["Dave Thomas","Andy Hunt","Chad Flower"],"Programming Ruby","The Facets of Ruby","Pragmatic Bookshelf",4,"07-jul-13",["ISBN-13: 978-1937785499","1937785491"] #Instancia de la clase 
		#con todos los atributos, a modo de prueba 
	end 
	
	describe "new" do
		it "Recibe 6 parametros y retorna un objeto" do
			@r.should be_an_instance_of Biblio
		end
	end
  	describe "Comprobacion de existencia de Parametros" do
		it "Existe uno o mas autores" do
			expect(@r.autores.length).not_to be 0
		end
		
		it "Existe un título" do
			expect(@r.titulo).to eq("Programming Ruby")
		end
		
		it "Existe o no una serie" do
			expect(@r.serie).to eq("The Facets of Ruby")
		end
		
		it "Existe una editorial" do
			expect(@r.editorial).to eq("Pragmatic Bookshelf")
		end
		
		it "Existe un numero de edicion" do
			expect(@r.edicion).to eq(4)
		end
		
		it "Existe fecha de publicacion" do
			expect(@r.fecha).to eq("07-jul-13")
		end
		
		it "Existe uno o mas ISBN" do
		expect(@r.isbn.length).not_to be 0
		end
   end
   
   
   describe "#Comprobacion de existencia de metodos" do
   	it "Existe metodo que obtiene lista de autores" do
   		 expect(@r.respond_to?(:autores)).to be true
   	end
   	
   	it "Existe metodo que obtiene titulo" do
   		expect(@r.respond_to?(:titulo)).to be true
   	end
   	
   	it "Existe metodo que obtiene serie" do
   		expect(@r.respond_to?(:serie)).to be true
   	end
   	
   	it "Existe metodo que obtiene editorial" do
   		expect(@r.respond_to?(:editorial)).to be true
    end
    
    it "Existe metodo que obtiene num. edicion" do
    	expect(@r.respond_to?(:edicion)).to be true
   	end
   	
   	it "Existe metodo que obtiene listado ISBN" do
   		expect(@r.respond_to?(:isbn)).to be true
   	end
=begin
   	it "Existe metodo que obtiene referencia formateada" do
   		expect(@r.to_s).to eq("Dave Thomas Andy Hunt Chad Flower\nProgramming Ruby\n The Facets of Ruby\n Pragmatic Bookshelf\n 4 07-jul-13\n ISBN-13: 978-1937785499 1937785491")
   	end
=end
   end
end