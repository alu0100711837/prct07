require 'refer'
require './lib/refer/list.rb'
require './lib/refer/node.rb'

include Refer

describe Refer do
    before :all do
        @refer1 = Biblio.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide", "The Facets of Ruby", "Pragmatic Bookshelf", "4 edition", "(July 7, 2013)", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
        @refer2 = Biblio.new(["Scott Chacon"], "Pro Git 2009th Edition", "Pro", "Apress", "2009 edition", "August 27, 2009", ["ISBN-13: 978-1430218333", "ISBN-10: 1430218339"])
        @refer3 = Biblio.new(["David Flanagan", "Yukihiro Matsumoto"], "The Ruby Programming Language", "Serie", "O’Reilly Media", " 1 edition", "February 4, 2008", ["ISBN-10: 0596516177", "ISBN-13: 978-0596516178"])
        @refer4 = Biblio.new(["David Chelimsky", "Dave Astels", " Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"], "The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends", "The Facets of Ruby", "Pragmatic Bookshelf", "1 edition", "December 25, 2010", ["ISBN-10: 1934356379", "ISBN-13: 978-1934356371"])
        @refer5 = Biblio.new(["Richard E. Silverman"], "Git Pocket Guide", "Serie", "O’Reilly Media", "1 edition", "August 2, 2013", ["ISBN-10: 1449325866", "ISBN-13: 978-1449325862"])
        
        @node1 = Node.new(@refer1)
        @node2 = Node.new(@refer2)
        @node3 = Node.new(@refer3)
        @node4 = Node.new(@refer4)
        @node5 = Node.new(@refer5)
        
        @list = List.new()
    end
    
    describe "Node" do
        it "Debe existir un nodo y su siguiente" do
            expect(@node1.value).to eq(@refer1)
            expect(@node1.next).to eq(nil)
        end
    end

    describe "List" do
        it "Se extrae el primer elemento de la lista" do
            @list.push_front(@refer1)
            @list.push_front(@refer2)
            @list.pop_front()
            expect(@list.ini.value).to eq(@refer1)
        end

        it "Se extrae el ultimo elemento de la lista" do
            @list.push_front(@refer1)
            @list.push_front(@refer2)
            @list.pop_back()
            expect(@list.ini.value).to eq(@refer2)
        end
    
        it "Se puede insertar un elemento por el inicio" do
            @list.push_front(@refer1)
            expect(@list.ini.value).to eq(@refer1)
        end
        
        it "Se puede insertar un elemento por el final" do
            @list.push_back(@refer1)
            expect(@list.ini.value).to eq(@refer1)
        end
        
        it "Se pueden insertar varios elementos" do
           @list.push_front(@refer1)
            @list.push_front(@refer2)
            @list.push_front(@refer3)
            @list.push_front(@refer4)
            @list.push_front(@refer5)
           expect(@list.ini.value).to eq(@refer5)
        end
        
        it "Debe existir una lista con su cabeza" do
            #expect(@list).to exist
            #expect(@list.ini).to exist
        end
    end
        
end